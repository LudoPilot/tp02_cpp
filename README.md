# tp02_cpp

Ceci est ma version du TP 2. Il contient des classes Point, Rectangle, Cercle, Triangle et Carré. Chaque classe a différentes méthodes.

Dans main.cpp, on peut modifier les coordonnées des points et les propriétés des rectangles (longueur, largeur), du cercle (diamètre), du triangle (3ème sommet) et du carré (largeur).

POINT
Point point1{0.0, 0.0};
Point point2{4.0, 0.0};

RECTANGLES
Rectangle monRectangle(5, 10, 2.0, 3.0);
Rectangle deuxiemeRectangle(8, 12, 1.0, 2.0);

TRIANGLE
Triangle monTriangle(point1, point2, Point{2.0, 3.0});

CERCLE
Cercle monCercle(point1, 6.0);
Point unPoint{7.0, 2.0};

CARRÉ
Carre monCarre(7.0, 2.0, 3.0);