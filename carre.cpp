#include "carre.h"
#include <iostream>

Carre::Carre(float cote, float x, float y) : Rectangle(cote, cote, x, y) {}

float Carre::CalculerCote() const {
    return GetLongueur();
}

bool Carre::ComparerCotes(const Carre& autreCarre) const {
    return CalculerCote() > autreCarre.CalculerCote();
}

bool Carre::ComparerPerimetres(const Carre& autreCarre) const {
    return CalculerPerimetre() > autreCarre.CalculerPerimetre();
}

bool Carre::ComparerSurfaces(const Carre& autreCarre) const {
    return CalculerSurface() > autreCarre.CalculerSurface();
}

void Carre::Afficher() const {
    std::cout << "Côté : " << CalculerCote() << std::endl;
    Point coinSupG = GetCoinSuperieurGauche();
    std::cout << "Coin supérieur gauche : (" << coinSupG.x << ", " << coinSupG.y << ")" << std::endl;
    std::cout << "Surface : " << CalculerSurface() << std::endl;
    std::cout << "Périmètre : " << CalculerPerimetre() << std::endl;
}
