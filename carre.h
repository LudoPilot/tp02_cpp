#ifndef CARRE_H
#define CARRE_H

#include "rectangle.h"

class Carre : public Rectangle {
public:
    Carre(float cote, float x, float y);

    float CalculerCote() const;
    bool ComparerCotes(const Carre& autreCarre) const;
    bool ComparerPerimetres(const Carre& autreCarre) const;
    bool ComparerSurfaces(const Carre& autreCarre) const;
    void Afficher() const;
};

#endif
