#include "cercle.h"
#include <cmath>

Cercle::Cercle(const Point& centre, float diametre) : Centre(centre), Diametre(diametre) {}

Point Cercle::GetCentre() const {
    return Centre;
}

float Cercle::GetDiametre() const {
    return Diametre;
}

void Cercle::SetCentre(const Point& centre) {
    Centre = centre;
}

void Cercle::SetDiametre(float diametre) {
    Diametre = diametre;
}

double Cercle::CalculerPerimetre() const {
    return M_PI * Diametre;
}

double Cercle::CalculerSurface() const {
    return M_PI * pow(Diametre / 2.0, 2);
}

bool Cercle::EstSurLeCercle(const Point& point) const {
    double distanceAuCentre = sqrt(pow(point.x - Centre.x, 2) + pow(point.y - Centre.y, 2));
    return fabs(distanceAuCentre - Diametre / 2.0) < 1e-6;
}

bool Cercle::EstAInterieur(const Point& point) const {
    double distanceAuCentre = sqrt(pow(point.x - Centre.x, 2) + pow(point.y - Centre.y, 2));
    return distanceAuCentre < Diametre / 2.0;
}

void Cercle::Afficher() const {
    std::cout << "Centre : (" << Centre.x << ", " << Centre.y << ")" << std::endl;
    std::cout << "Diamètre : " << Diametre << std::endl;
    std::cout << "Périmètre : " << CalculerPerimetre() << std::endl;
    std::cout << "Surface : " << CalculerSurface() << std::endl;
}
