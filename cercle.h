#ifndef CERCLE_H
#define CERCLE_H

#include "Point.h"

class Cercle {
private:
    Point Centre;
    float Diametre;

public:
    Cercle(const Point& centre, float diametre);

    Point GetCentre() const;
    float GetDiametre() const;
    void SetCentre(const Point& centre);
    void SetDiametre(float diametre);

    double CalculerPerimetre() const;
    double CalculerSurface() const;
    bool EstSurLeCercle(const Point& point) const;
    bool EstAInterieur(const Point& point) const;

    void Afficher() const;
};

#endif
