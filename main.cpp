//
// Created by Ludo on 11/10/2023.
//

#include "point.h"
#include "rectangle.h"
#include "cercle.h"
#include "triangle.h"
#include "carre.h"
#include <iostream>

int main() {
    // POINT
    Point point1{0.0, 0.0};
    Point point2{4.0, 0.0};

    point1.Afficher();
    point2.Afficher();

    // RECTANGLE
    Rectangle monRectangle(5, 10, 2.0, 3.0);
    Rectangle deuxiemeRectangle(8, 12, 1.0, 2.0);

    monRectangle.Afficher();
    deuxiemeRectangle.Afficher();

    int aire1 = monRectangle.CalculerSurface();
    int aire2 = deuxiemeRectangle.CalculerSurface();

    bool comparaison = monRectangle.ComparerPerimetres(deuxiemeRectangle);

    std::cout << "Aire du premier rectangle : " << aire1 << std::endl;
    std::cout << "Aire du deuxième rectangle : " << aire2 << std::endl;

    if (comparaison) {
        std::cout << "Le premier rectangle a un périmètre plus grand que le deuxième." << std::endl;
    } else {
        std::cout << "Le premier rectangle n'a pas un périmètre plus grand que le deuxième." << std::endl;
    }

    // CERCLE
    Cercle monCercle(point1, 6.0);

    monCercle.Afficher();

    double perimetreCercle = monCercle.CalculerPerimetre();
    double surfaceCercle = monCercle.CalculerSurface();

    Point unPoint{7.0, 2.0};
    bool estSurLeCercle = monCercle.EstSurLeCercle(unPoint);
    bool estAInterieur = monCercle.EstAInterieur(unPoint);

    std::cout << "Périmètre du cercle : " << perimetreCercle << std::endl;
    std::cout << "Surface du cercle : " << surfaceCercle << std::endl;

    if (estSurLeCercle) {
        std::cout << "Le point est sur le cercle." << std::endl;
    } else {
        std::cout << "Le point n'est pas sur le cercle." << std::endl;
    }

    if (estAInterieur) {
        std::cout << "Le point est à l'intérieur du cercle." << std::endl;
    } else {
        std::cout << "Le point n'est pas à l'intérieur du cercle." << std::endl;
    }

    // TRIANGLE
    Triangle monTriangle(point1, point2, Point{2.0, 3.0});

    monTriangle.Afficher();

    double baseTriangle = monTriangle.CalculerBase();
    double hauteurTriangle = monTriangle.CalculerHauteur();
    double surfaceTriangle = monTriangle.CalculerSurface();
    bool estIsocele = monTriangle.VerifierSiIsocele();
    bool estRectangle = monTriangle.VerifierSiRectangle();
    bool estEquilateral = monTriangle.VerifierEquilateral();

    std::cout << "Base du triangle : " << baseTriangle << std::endl;
    std::cout << "Hauteur du triangle : " << hauteurTriangle << std::endl;
    std::cout << "Surface du triangle : " << surfaceTriangle << std::endl;

    if (estIsocele) {
        std::cout << "Le triangle est isocèle." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas isocèle." << std::endl;
    }

    if (estRectangle) {
        std::cout << "Le triangle est rectangle." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas rectangle." << std::endl;
    }

    if (estEquilateral) {
        std::cout << "Le triangle est équilatéral." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas équilatéral." << std::endl;
    }

    // CARRÉ
    Carre monCarre(7.0, 2.0, 3.0);
    Carre deuxiemeCarre(5.0, 1.0, 2.0);

    monCarre.Afficher();
    deuxiemeCarre.Afficher();

    float coteCarre = monCarre.CalculerCote();
    bool comparaisonCotes = monCarre.ComparerCotes(deuxiemeCarre);
    bool comparaisonPerimetres = monCarre.ComparerPerimetres(deuxiemeCarre);
    bool comparaisonSurfaces = monCarre.ComparerSurfaces(deuxiemeCarre);

    std::cout << "Côté du premier carré : " << coteCarre << std::endl;

    if (comparaisonCotes) {
        std::cout << "Le premier carré a un côté plus grand que le deuxième carré." << std::endl;
    } else {
        std::cout << "Le premier carré n'a pas un côté plus grand que le deuxième carré." << std::endl;
    }

    if (comparaisonPerimetres) {
        std::cout << "Le premier carré a un périmètre plus grand que le deuxième carré." << std::endl;
    } else {
        std::cout << "Le premier carré n'a pas un périmètre plus grand que le deuxième carré." << std::endl;
    }

    if (comparaisonSurfaces) {
        std::cout << "Le premier carré a une surface plus grande que le deuxième carré." << std::endl;
    } else {
        std::cout << "Le premier carré n'a pas une surface plus grande que le deuxième carré." << std::endl;
    }

    return 0;
}

