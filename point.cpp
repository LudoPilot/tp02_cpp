#include <iostream>
#include "point.h"

Point::Point(float xCoord, float yCoord) : x(xCoord), y(yCoord) {}

void Point::Afficher() const {
    std::cout << "Coordonnées du point : (" << x << ", " << y << ")" << std::endl;
}