#ifndef POINT_H
#define POINT_H

#include <iostream>

struct Point {
    float x;
    float y;

    Point(float xCoord, float yCoord);

    void Afficher() const;
};

#endif
