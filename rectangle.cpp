#include "rectangle.h"
#include <cmath>
#include <iostream>

Rectangle::Rectangle(int longueur, int largeur, float x, float y)
        : Longueur(longueur), Largeur(largeur), CoinSuperieurGauche(Point{x, y}) {}

float Rectangle::GetLongueur() const {
    return Longueur;
}

float Rectangle::GetLargeur() const {
    return Largeur;
}

Point Rectangle::GetCoinSuperieurGauche() const {
    return CoinSuperieurGauche;
}

void Rectangle::SetLongueur(int longueur) {
    Longueur = longueur;
}

void Rectangle::SetLargeur(int largeur) {
    Largeur = largeur;
}

void Rectangle::SetCoinSuperieurGauche(float x, float y) {
    CoinSuperieurGauche.x = x;
    CoinSuperieurGauche.y = y;
}

float Rectangle::CalculerSurface() const {
    return Longueur * Largeur;
}

float Rectangle::CalculerPerimetre() const {
    return 2 * (Longueur + Largeur);
}

bool Rectangle::ComparerPerimetres(const Rectangle& deuxiemeRectangle) const {
    int perimetreRectangle = CalculerPerimetre();
    int perimetreDeuxiemeRectangle = deuxiemeRectangle.CalculerPerimetre();
    return perimetreRectangle > perimetreDeuxiemeRectangle;
}

void Rectangle::Afficher() const {
    std::cout << "Longueur : " << GetLongueur() << std::endl;
    std::cout << "Largeur : " << GetLargeur() << std::endl;
    Point coinSupG = GetCoinSuperieurGauche();
    std::cout << "Coin supérieur gauche : (" << coinSupG.x << ", " << coinSupG.y << ")" << std::endl;
    std::cout << "Surface : " << CalculerSurface() << std::endl;
    std::cout << "Périmètre : " << CalculerPerimetre() << std::endl;
}
