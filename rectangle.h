#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Point.h"

class Rectangle {
private:
    float Longueur;
    float Largeur;
    Point CoinSuperieurGauche;

public:
    Rectangle(int longueur, int largeur, float x, float y);

    float GetLongueur() const;
    float GetLargeur() const;
    Point GetCoinSuperieurGauche() const;

    void SetLongueur(int longueur);
    void SetLargeur(int largeur);
    void SetCoinSuperieurGauche(float x, float y);

    float CalculerSurface() const;
    float CalculerPerimetre() const;
    bool ComparerPerimetres(const Rectangle& deuxiemeRectangle) const;
    void Afficher() const;
};

#endif

