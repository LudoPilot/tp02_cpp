#include "triangle.h"
#include <iostream>
#include <cmath>
#include <algorithm>

Triangle::Triangle(const Point& p1, const Point& p2, const Point& p3)
        : Point1(p1), Point2(p2), Point3(p3) {}

Point Triangle::GetPoint1() const {
    return Point1;
}

Point Triangle::GetPoint2() const {
    return Point2;
}

Point Triangle::GetPoint3() const {
    return Point3;
}

void Triangle::SetPoint1(const Point& p1) {
    Point1 = p1;
}

void Triangle::SetPoint2(const Point& p2) {
    Point2 = p2;
}

void Triangle::SetPoint3(const Point& p3) {
    Point3 = p3;
}

double Triangle::CalculerBase() const {
    double longueur1 = CalculerLongueurCote(Point1, Point2);
    double longueur2 = CalculerLongueurCote(Point2, Point3);
    double longueur3 = CalculerLongueurCote(Point3, Point1);
    return std::max(std::max(longueur1, longueur2), longueur3);
}

double Triangle::CalculerHauteur() const {
    double surface = CalculerSurface();
    double base = CalculerBase();
    return 2 * surface / base;
}

double Triangle::CalculerSurface() const {
    double longueur1 = CalculerLongueurCote(Point1, Point2);
    double longueur2 = CalculerLongueurCote(Point2, Point3);
    double longueur3 = CalculerLongueurCote(Point3, Point1);
    double s = (longueur1 + longueur2 + longueur3) / 2.0;
    return sqrt(s * (s - longueur1) * (s - longueur2) * (s - longueur3));
}

void Triangle::Afficher() const {
    std::cout << "Point 1 : (" << Point1.x << ", " << Point1.y << ")" << std::endl;
    std::cout << "Point 2 : (" << Point2.x << ", " << Point2.y << ")" << std::endl;
    std::cout << "Point 3 : (" << Point3.x << ", " << Point3.y << ")" << std::endl;
    std::cout << "Base : " << CalculerBase() << std::endl;
    std::cout << "Hauteur : " << CalculerHauteur() << std::endl;
    std::cout << "Surface : " << CalculerSurface() << std::endl;
}

double Triangle::CalculerLongueurCote(const Point& p1, const Point& p2) const {
    return sqrt(pow(p2.x - p1.x, 2) + pow(p2.y - p1.y, 2));
}

bool Triangle::VerifierSiIsocele() const {
    double longueur1 = CalculerLongueurCote(Point1, Point2);
    double longueur2 = CalculerLongueurCote(Point2, Point3);
    double longueur3 = CalculerLongueurCote(Point3, Point1);

    return (longueur1 == longueur2) || (longueur2 == longueur3) || (longueur3 == longueur1);
}

bool Triangle::VerifierSiRectangle() const {
    double longueur1 = CalculerLongueurCote(Point1, Point2);
    double longueur2 = CalculerLongueurCote(Point2, Point3);
    double longueur3 = CalculerLongueurCote(Point3, Point1);

    double longueurs[3] = {longueur1, longueur2, longueur3};
    std::sort(longueurs, longueurs + 3);

    return (pow(longueurs[0], 2) + pow(longueurs[1], 2)) == pow(longueurs[2], 2);
}

bool Triangle::VerifierEquilateral() const {
    double longueur1 = CalculerLongueurCote(Point1, Point2);
    double longueur2 = CalculerLongueurCote(Point2, Point3);
    double longueur3 = CalculerLongueurCote(Point3, Point1);

    return (longueur1 == longueur2) && (longueur2 == longueur3);
}
