#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Point.h"

class Triangle {
private:
    Point Point1;
    Point Point2;
    Point Point3;

public:
    Triangle(const Point& p1, const Point& p2, const Point& p3);

    Point GetPoint1() const;
    Point GetPoint2() const;
    Point GetPoint3() const;

    void SetPoint1(const Point& p1);
    void SetPoint2(const Point& p2);
    void SetPoint3(const Point& p3);

    double CalculerBase() const;
    double CalculerHauteur() const;
    double CalculerSurface() const;
    void Afficher() const;
    double CalculerLongueurCote(const Point& p1, const Point& p2) const;
    bool VerifierSiIsocele() const;
    bool VerifierSiRectangle() const;
    bool VerifierEquilateral() const;
};

#endif
